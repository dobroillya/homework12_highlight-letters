const btns = document.querySelectorAll(".btn");

function checkBtn(key) {
  if(key !== 'Enter') {
    key = key.toUpperCase();
  }
  
  btns.forEach((item) => {
    item.classList.remove('active')
    if (item.textContent === key) {        
      item.classList.add('active')
    }
  });
}

document.addEventListener("keydown", (e) => {
  checkBtn(e.key);
});
